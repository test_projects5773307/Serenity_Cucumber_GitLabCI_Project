Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "apple", "orange", "pasta", "cola"
### Prepare Positive and negative scenarios

  Scenario Outline: Search for the certain product
    When user searches the "<product>"
    Then user should see the results displayed for "<product>"
    Examples:
      | product |
      | apple   |
      | orange  |
      | cola    |

  Scenario: Search for the non-existing product - tablet
    When user searches the non-existing "tablet"
    Then user should not see the results displayed for tablet

  Scenario: Verify fields are not empty
    When user searches the "pasta"
    Then user should see the results with the following fields: "provider,title,url,brand,price,unit,image"

  Scenario: Verify price is greater than 0.99 €
    When user searches the "pasta"
    Then user should see the price of products is greater than 0.99 €



