package starter.apiClients;

import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;


public class ProductApiClient {
    private static final EnvironmentVariables environmentVariables = new SystemEnvironmentVariables();
    private static final String baseUrl;

    static {
        baseUrl = environmentVariables.optionalProperty("restapi.baseurl")
                .orElse("https://waarkoop-server.herokuapp.com/api/v1/search/demo/");
    }


    public Response getProducts(String product) {
        return SerenityRest.get(baseUrl + product);
    }

}
