package starter.utils;

import java.util.Arrays;
import java.util.Optional;

public class EnumUtils {
    public static <T extends Enum> T getEnumByValueContainsEnumName(Class<T> clazz, String value) {
        T[] enums = clazz.getEnumConstants();
        Optional<T> result = Arrays.stream(enums)
                .filter(p -> value.toLowerCase().trim().contains(p.name().toLowerCase().trim()))
                .findFirst();
        if (result.isPresent()) {
            return result.get();
        } else {
            throw new EnumConstantNotPresentException(clazz, value);
        }
    }
}
