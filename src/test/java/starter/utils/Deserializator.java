package starter.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import starter.objects.Product;

import java.util.ArrayList;
import java.util.List;

public class Deserializator {
    /**
     * Converting json string to Product class object
     *
     * @param jsonString
     * @return list of Products
     */

    public List<Product> deserializeJSON(String jsonString) {
        ObjectMapper mapper = new ObjectMapper();
        List<Product> prList = new ArrayList<>();
        try {
            prList = mapper.readValue(jsonString, new TypeReference<List<Product>>() {
            });
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return prList;
    }

}
