package starter.objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class Product {
    private String provider;
    private String title;
    private String url;
    private String brand;
    private double price;
    private String unit;
    @JsonProperty("isPromo")
    private boolean isPromo;
    private String promoDetails;
    private String image;

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public boolean isPromo() {
        return isPromo;
    }

    public void setPromo(boolean promo) {
        isPromo = promo;
    }

    public String getPromoDetails() {
        return promoDetails;
    }

    public void setPromoDetails(String promoDetails) {
        this.promoDetails = promoDetails;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return provider.equals(product.provider) && title.equals(product.title) && url.equals(product.url) && brand.equals(product.brand) && image.equals(product.image);
    }

    @Override
    public int hashCode() {
        return Objects.hash(provider, title, url, brand, image);
    }
}
