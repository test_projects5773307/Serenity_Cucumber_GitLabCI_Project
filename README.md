
### Pre-Installations to run tests on your local machine:
1. Install integrated development environment (IDE) - Intellij idea
2. Install Java Development Kit (JDK)
3. Add `JAVA_HOME` variable to environment variable and add path to your jdk binary - `bin` file to `Path` environment variable on your OS
4. Install Maven
5. Add `MAVEN_HOME` variable to environment variable and add path to your maven binary - `bin` file to `Path` environment variable on your OS
### How to run tests on your local machine:
1. Import project folder to your integrated development environment (IDE)
or clone it from GitLub [**link**](https://gitlab.com/test_projects5773307/Serenity_Cucumber_GitLabCI_Project.git)
2. Use `mvn clean verify` to run with default environment, 
or use `mvn clean verify -Ddev` if you want to run with a specific environment. Where `-D` is the key 
and `dev`, `prod` are environments you can select to run with. Which environments are available you can find in the 
`<profiles></profiles>` section of the `pom.xml` file.
3. Open report link after tests are being executed
![img_1.png](images/img_1.png)

### How to add a new tests:
1. Go to `src/test/resources/features/search`
2. Create new file with `.feature` extension. For example: `product_search.feature`
3. Inside `product_search.feature` file define Feature and Scenario.
4. Define steps using next `Given, When, Then, And` or `But` keywords.
   ![img.png](images/img.png)
5. Go to `src/test/java/starter/stepdefinitions`
6. Create class that represents in our case search of products. For example: `[SearchStepDefinitions.java]`
7. Inside the class create methods that represent steps defined in the `.feature` file
and annotate them according to the keywords that has been used - `@When`, `@Then` and so on.
And provide to the annotation step description to identify the method is going to be used.
![img.png](images/img3.png)

### Refactored:
- Added `ProductApiClient` class (to initialize url and handle request itself)
- Added `Deserializator` class (to convert JSON into Product object)
- Added `ProductsKeywords` enum (to handle exception words. For example: we need to find `orange`,
but it can be represented also by `sinaasappel`)
- Added `Product` class (represents object product)
- Added `EnumUtils` class (to handle workflows with enum)
- Changed cucumber scenario to Outline (to run test with multiple parameters)
- Removed gradle build tool (we use Maven)
- Removed `CarsAPI`class (unnecessary class)
- Removed `.github` folder (we don't need GitHub Actions workflows)
- Changed `SearchStepDefinitions` class (according to the new scenarios)
- Added `gitlab-ci.yml` file (to configure GitLab CI)
- Added `<profiles></profiles>` to `pom.xml` (to handle multiple environments)
